const { Router } = require('express');
const DevController = require('./controllers/DevController')
const SearchControler = require('./controllers/SearchControler')

const routes = Router();

// Métodos HTTP
// get, post, put, delete

// Tipos de parametros:
// Query Params: request.query (Filtros, ordenação, paginação, ...)
// Route Params: request.params (Identificar um recurso na alteraçÃo ou remoção
// Body: request.body (Dados para criação ou alteração de um registro)

//MongoDB (banco de dados não relacional)

//index, show, store, update , destroy
 
routes.get('/devs', DevController.index);
routes.post('/devs', DevController.store);

routes.get('/search', SearchControler.index)

module.exports = routes;